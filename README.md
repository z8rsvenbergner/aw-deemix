You can run the Program directly from your Command-Line.

## docker run -it --rm -v "$PWD/downloads:/root/Music/deemix Music/" registry.gitlab.com/z8rsvenbergner/aw-deemix /bin/bash ##

then you are in the container
use the tool with the followed line

## deemix <URL> ##

when everything is ready, you must exit the container

## exit ##

the files will now download now and will be in your actual working directory under "downloads"

## cd downloads ##

### Thats it! ###
